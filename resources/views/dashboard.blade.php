@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a href="/posts/create" class="btn btn-primary mb-3"> Create Post</a>
                    <h3>Create You Blog!</h3>
                    @if(count($posts))
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tile</th>
                                        <th> Body</th>
                                        <th colspan="2"></th>
                                    </tr>
                                </thead>
                                @foreach( $posts as $post)
                                    <tbody>
                                        <tr>
                                            <td> {{ $post->id }} </td>
                                            <td> {{ $post->title }} </td>
                                            <td> {!! $post->body !!} </td>
                                            <td> <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit</a> </td>
                                            <td>
                                                {!! Form::open(['action'=>['PostsController@destroy', $post->id],'class'=>'float-right']) !!}
                                                {{ form::submit('Delete',['class'=>'btn btn-danger'])}}
                                                {{ form::hidden('_method','DELETE')}}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    </tbody>
                                @endforeach
                            </table>
                    @else
                        <p> No Post</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
