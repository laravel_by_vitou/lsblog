@extends('layouts.app')

@section('content')

    <h1> Create Post </h1>
    {!! Form::open(['action'=>['PostsController@update',$posts->id],'method'=>'post','enctype'=>'multipart/form-data']) !!}
       {{ form::label('title','Title')}}
       {{ form::text('title', $posts->title ,['class'=>'form-control','placeholder'=>'Title'])}}
       {{ form::label('body','Body')}}
       {{ form::Textarea('body',$posts->body,['id'=>'article-ckeditor','class'=>'form-control', 'placeholder'=>'Text Body'])}}
       <div class="form-group mt-3">
            {{ form::file('cover_image', array('value' => $posts->cover_image ))}}
        </div>
       {{ form::hidden('_method','PUT')}}
       {{ form::submit('post',['class'=>'btn btn-success mt-2'])}}
    {!! Form::close() !!}   
    
@endsection