@extends('layouts.app')

@section('content')
    <a href="/posts" class="btn btn-secondary"> Go back </a>
    <h1>{{ $posts->title}}</h1>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <img class="img-fluid" src="/storage/storage/{{$posts->cover_image}}" alt="cover image">
                    <p>{!! $posts->body !!}</p>
                    <hr>
                    <small> write on {{ $posts->created_at}} by {{$posts->user->name}}</small>
                    @if( !Auth::guest())
                        @if( auth()->user()->id == $posts->user_id )
                            <hr>
                            <a href="/posts/{{$posts->id}}/edit" class="btn btn-primary">Edit</a>
                            {!! Form::open(['action'=>['PostsController@destroy', $posts->id],'class'=>'float-right']) !!}
                                {{ form::submit('Delete',['class'=>'btn btn-danger'])}}
                                {{ form::hidden('_method','DELETE')}}
                            {!! Form::close() !!}
                        @endif
                    @endif
                </div>
                <div class="col-md-4 col-sm-4">
                    <a href="/like/{{$posts->id}}" class="btn btn-primary">10&nbsp;Likes </a>
                    <span>
                        <a href="/posts" class="btn btn-danger">10&nbsp;Unlikes </a>
                    </span>
                </div>
            </div>
        </div>
    </div>
    
    <br><br>
    <br style="clear:both;">
@endsection