@extends('layouts.app')

@section('content')

    <h1> Posts</h1>

    @if( count($posts) > 0 )
        @foreach( $posts as $post )
            <div class="card mb-2">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <img class="img-thumbnail" src="/storage/storage/{{$post->cover_image}}" alt="cover image">
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <h3><a href="/posts/{{$post->id}}">{{ $post->title }}</a></h3>
                            <small> Written on {{ $post->created_at }} by {{$post->user->name}}</small>       
                        </div>                              
                    </div>
                </div>
            </div>
        @endforeach
        {{ $posts->links()}}
    @else   
        <p> No post</p>
    @endif
@endsection