
@extends('layouts.app')

@section('content')
       <h1>{{$title}}</h1>
       <div class="card">
           <div class="card-body">
               <div class="row">
                   <div class="col-md-4 col-sm-4">
                        <img style="width:100%" src="/storage/admin/vitou_pak.jpg" alt="cover image">
                   </div>
                   <div class="col-md-8 col-sm-8">
                        <h5 class="card-title">Name: <strong>PAK Vitou </strong> </h5>
                        <p class="card-text"> Skill: <strong> Web Application Developer</strong></p>
                        <p class="card-text"> Email: <strong> Pakvitou168@gmail.com</strong></p>
                        <p class="card-text"> TEL: <strong> 096 23 59 162</strong></p>
                        <br>
                        <a href="https://www.facebook.com/vitou.pak.1" target="_blank" class="btn btn-primary"><span><i class="fab fa-facebook-f fa-2x"></i></span> Facebook</a>
                        <span>
                                <a href="https://www.youtube.com/channel/UCNgN7k7tEtilI8uR3HPJ0Rg?view_as=subscriber" target="_blank" class="btn btn-danger"><span><i class="fab fa-youtube-square fa-2x"></i></span> Facebook</a>
                        </span>
                        
                   </div>
               </div>
           </div>
       </div>
       {{-- <p>About page</p> --}}
@endsection
