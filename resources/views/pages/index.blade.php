
@extends('layouts.app')

@section('content')

    <div class="jumbotron text-center">
        <h1>{{$title}}</h1>
        <p> This is index page </p>
        <p>
            <a href="#" class="btn btn-primary btn-lg"> login</a> <a href="#" class="btn btn-secondary btn-lg"> Register </a></p>
    </div>
@endsection
      