<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Storage;

class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except'=> ['index','show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $posts = Post::all();
        // $posts = Post::orderBy('id','desc')->get();
        $posts = Post::orderBy('id','desc')->paginate(10);

        return view('posts.index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate( $request,[
            'title' =>'required',    
            'body' =>'required', 
            'cover_image'=>'image|nullable|max:1999'
              
        ]);
        // check image upload
        if( $request->hasFile('cover_image')){
            // get filename with extension
             $fileNameWithExt = $request->file('cover_image')->getClientOriginalName();
            //  get fileName
            $fileName = pathinfo( $fileNameWithExt, PATHINFO_FILENAME);
            // get Ext
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            // filenameToStore 
            $filenameToStore = $fileName.'_'.time().'.'.$extension; 
            // upload image
            $path = $request->file('cover_image')->storeAs('Public/storage', $filenameToStore);
        }else{
            $filenameToStore = 'noimage.jpg';
        }
        // create post
        $post = new Post;
        $post->title = $request->input('title');      
        $post->body = $request->input('body'); 
        $post->user_id = auth()->user()->id;
        $post->cover_image = $filenameToStore;
        $post->save();
        
        return redirect('/posts')->with('success', 'Post Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $posts = Post::find($id);

        // check specified Post
        if( $posts == null )
        {
            return redirect('/posts')->with('error','Page Not Found');
        }

        return view('posts.show')->with('posts', $posts);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $posts = Post::find($id);

        if( auth()->user()->id !== $posts->user_id)
        {
            return redirect('/posts')->with('error', 'Unauthorized Page');
        }
        return view('posts.edit')->with('posts', $posts);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate( $request,[
            'title' =>'required',    
            'body' =>'required',  
            'cover_image'=>'image|nullable|max:1999' 
        ]);
        
          // check image upload
        if( $request->hasFile('cover_image')){
            // get filename with extension
             $fileNameWithExt = $request->file('cover_image')->getClientOriginalName();
            //  get fileName
            $fileName = pathinfo( $fileNameWithExt, PATHINFO_FILENAME);
            // get Ext
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            // filenameToStore 
            $filenameToStore = $fileName.'_'.time().'.'.$extension; 
            // upload image
            $path = $request->file('cover_image')->storeAs('Public/storage', $filenameToStore);
        }
        // create post
        $post = Post::find($id);
        $post->title = $request->input('title');      
        $post->body = $request->input('body'); 
        $post->cover_image = $filenameToStore;
        $post->update();
        
        return redirect('/posts')->with('success', 'Post updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        
         // check specified user
        if( Auth()->user()->id != $post->user_id)
        {
            return redirect('/posts')->with('error', 'Unauthorized Page');
        }

        // check user image
        if( $post->cover_image != 'noimage')
        {
            storage::delete('public/storage/'.$post->cover_image);
        }

        $post->delete();
        return redirect('/posts')->with('success', 'Post removed');

    }
}
