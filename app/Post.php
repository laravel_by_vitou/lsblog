<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Post extends Model
{
    // Table name
    protected $table = 'posts';


    // relationship to user
    function user()
    {
        return $this->belongsTo('App\User');
    }
    
    function _user()
    {
        return $this->belongsToMany('App\User');
    }
}
